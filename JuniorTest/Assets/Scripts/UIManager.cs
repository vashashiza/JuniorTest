﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject MainMenuPanel = null;

    private static UIManager instanse;
    public static UIManager Instanse
    {
        get
        {
            if (instanse == null)
            {
                instanse = FindObjectOfType<UIManager>();
            }
            return instanse;
        }

        set
        {
            if (instanse == null)
            {
                instanse = value;
            }
            else if (instanse != null)
            {
                Destroy(value);
            }
        }
    }

    [SerializeField]
    private GameObject canvas = null;
    [SerializeField]
    private HealthBar healthBarPrefab = null;

    private List<Unit> unitsList = new List<Unit>();

    /// <summary>
    /// for button event
    /// </summary>
    /// <param name="number"></param>
    public void LoadLevel(int number)
    {
        SwitchMainMenu(false);
        if (SceneManager.sceneCount>1)
        {
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(1));
        }
        SceneManager.LoadSceneAsync(number, LoadSceneMode.Additive);
    }

    public void SetUnitList(List<Unit> units)
    {
        unitsList = units;
        foreach (var item in unitsList)
        {
            HealthBar healthBar = Instantiate(healthBarPrefab);
            healthBar.transform.SetParent(canvas.transform);
            healthBar.Target = item;
        }        
    }

    public void SwitchMainMenu(bool value)
    {
        if (value)
        {
            MainMenuPanel.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            MainMenuPanel.SetActive(false);
            Time.timeScale = 1;
        }
    }

    private void Awake()
    {
        Instanse = this;
        SwitchMainMenu(true);
    }
}
