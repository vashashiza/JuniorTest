﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private int minHealthPackCount = 3;
    [SerializeField]
    private int maxHealthPackCount = 5;

    [SerializeField]
    private Vector3 Point1 = new Vector3();
    [SerializeField]
    private Vector3 Point2 = new Vector3();

    [SerializeField]
    private Transform spawnPoint = null;

    [SerializeField]
    private int unitCount = 3;

    [SerializeField]
    private HealthPack healthPackPrefab = null;
    [SerializeField]
    private Unit unitPrefab = null;

    private List<Unit> unitsList = new List<Unit>();

    private int healthPacksCount;

    private Unit activeUnit;

    public Unit ActiveUnit
    {
        get
        {
            return activeUnit;
        }

        set
        {
            if (activeUnit)
            {
                activeUnit.SetSelected(false);
            }
            activeUnit = value;
            activeUnit.SetSelected(true);
        }
    }

    private void Awake()
    {
        spawnUnits(unitCount);
        spawnHealthPacks();
        UIManager.Instanse.SetUnitList(unitsList);
    }

    private void spawnHealthPacks()
    {
        int count = UnityEngine.Random.Range(minHealthPackCount, maxHealthPackCount + 1);
        healthPacksCount = count;
        while (count > 0)
        {
            Vector3 spawnPoint = new Vector3(UnityEngine.Random.Range(Point1.x, Point2.x), Point1.y, UnityEngine.Random.Range(Point1.z, Point2.z));

            if (!Physics.CheckSphere(spawnPoint, 0.5f))
            {
                HealthPack healthPack = Instantiate(healthPackPrefab, spawnPoint, healthPackPrefab.transform.rotation);
                healthPack.transform.SetParent(transform);
                healthPack.OnDisintegrate += HealthPack_OnDisintegrate;
                count--;
            }
        }
    }

    private void HealthPack_OnDisintegrate(HealthPack obj)
    {
        healthPacksCount--;
        if (healthPacksCount == 0)
        {
            endGame();
        }
    }

    private void endGame()
    {
        UIManager.Instanse.SwitchMainMenu(true);
    }

    private void spawnUnits(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Vector3 spawn = new Vector3(spawnPoint.position.x + i, spawnPoint.position.y, spawnPoint.position.z);
            Unit unit = Instantiate(unitPrefab, spawn, unitPrefab.transform.rotation);
            unit.transform.SetParent(transform);
            unitsList.Add(unit);
        }
        ActiveUnit = unitsList[0];
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo))
            {
                if (hitInfo.collider.tag == "Unit")
                {
                    ActiveUnit = hitInfo.transform.GetComponent<Unit>();
                }
                else if (ActiveUnit)
                {
                    ActiveUnit.Move(hitInfo.point);
                }
            }
        }
    }
}
