﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HealthPack : MonoBehaviour
{
    [SerializeField]
    private int[] pointValues = new int[] { 10, 25 };

    [SerializeField]
    private int points;
    public int Points
    {
        get
        {
            return points;
        }

        set
        {
            points = value;
        }
    }

    /// <summary>
    /// HealthPack sender
    /// </summary>
    public event Action<HealthPack> OnDisintegrate;
    private void riseOnDisintegrate()
    {
        if (OnDisintegrate != null) OnDisintegrate(this);
    }

    public void Disintegrate()
    {
        if (this)
        {
            riseOnDisintegrate();
            Destroy(gameObject);
        }
    }

    private void Awake()
    {
        points = pointValues[UnityEngine.Random.Range(0, pointValues.Length)];       
    }
}
