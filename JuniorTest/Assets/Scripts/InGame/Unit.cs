﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unit : MonoBehaviour
{
    [SerializeField]
    private Animator animator = null;
    [SerializeField]
    private NavMeshAgent navMeshAgent = null;
    [SerializeField]
    private Renderer rend = null;

    [SerializeField]
    private Color selectedColor = new Color(0, 1, 0);
    [SerializeField]
    private Color unSelectedColor = new Color(1, 1, 1);

    [SerializeField]
    private float maxHitPoints = 100;
    [SerializeField]
    private float hitPoints;

    public float HitPoints
    {
        get
        {
            return hitPoints;
        }

        private set
        {
            if (value > maxHitPoints)
            {
                hitPoints = maxHitPoints;
            }
            else
            {
                hitPoints = value;
            }
        }
    }

    /// <summary>
    /// Unit sender
    /// </summary>
    public event Action<Unit> OnHitpoitschange;

    private void riseOnHitpoitschange()
    {
        if (OnHitpoitschange != null) OnHitpoitschange(this);
    }

    public float CurrentHitPointsRatio
    {
        get
        {
            return hitPoints / maxHitPoints;
        }
    }

    public void Move(Vector3 target)
    {
        animator.SetBool("Walk", true);
        navMeshAgent.SetDestination(target);
    }

    public void SetSelected(bool value)
    {
        if (value)
        {
            rend.material.color = selectedColor;
        }
        else
        {
            rend.material.color = unSelectedColor;
        }
    }

    private void Awake()
    {
        HitPoints = UnityEngine.Random.Range(10, 51);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "HealthPack")
        {
            HealthPack healthPack = other.GetComponent<HealthPack>();
            HitPoints += healthPack.Points;
            riseOnHitpoitschange();
            healthPack.Disintegrate();
        }
    }
    private void Update()
    {
        if (transform.position == navMeshAgent.destination)
        {
            animator.SetBool("Walk", false);
        }
    }
}
