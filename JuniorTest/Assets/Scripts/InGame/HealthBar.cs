﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    [SerializeField]
    private Image healthBarBackground = null;
    [SerializeField]
    private Image healths = null;

    [SerializeField]
    private float height = 1.8f;
    [SerializeField]
    private Vector3 ratio = new Vector3();

    [SerializeField]
    private Unit target;
    public Unit Target
    {
        set
        {
            target = value;
            setValue(target.CurrentHitPointsRatio);
            target.OnHitpoitschange += Target_OnHitpoitschange;
        }
    }

    private void setValue(float value)
    {
        float width = healthBarBackground.rectTransform.rect.width * value;
        healths.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, width);
    }

    private void Target_OnHitpoitschange(Unit obj)
    {
        setValue(obj.CurrentHitPointsRatio);
    }

    private void disintegrate()
    {
        if (this)
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        changePosition();
    }

    private void Start()
    {
        changePosition();
    }

    private void changePosition()
    {
        if (target == null)
        {
            disintegrate();
            return;
        }

        healthBarBackground.transform.localScale = ratio / Vector3.Distance(Camera.main.transform.position, target.transform.position);
        Vector3 healthBarWorldPosition = target.transform.position + new Vector3(0.0f, height, 0.0f);
        transform.position = Camera.main.WorldToScreenPoint(healthBarWorldPosition);
    }

#if UNITY_EDITOR
    [ContextMenu("SetDeltaSize")]
    public void SetDeltaSize()
    {
        float distance = Vector3.Distance(Camera.main.transform.position, target.transform.position);
        ratio = healthBarBackground.transform.localScale * distance;
    }
#endif
}
